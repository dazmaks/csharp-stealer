using System;

namespace Config
{
	class Config{

		public const string token = "TOKEN";
		public const string id = "ID";

		const string target = "default";
		public const string triggermessage = "target triggered:" + target;
	}
}
